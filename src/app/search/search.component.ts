import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

	cities: any[] = [];

  	constructor(
		private route: ActivatedRoute
  	) { }

	ngOnInit() {
		this.cities[0] = this.route.snapshot.params['key'];
	}

}
