import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserModule  } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './../home/home.component';
import { WeatherPageComponent } from './../weather-page/weather-page.component';
import { SearchComponent } from './../search/search.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'weather/:id', component: WeatherPageComponent },
  { path: 'search/:key', component: SearchComponent },
  { path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  declarations: []
})

export class RoutesModule { }
