import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-weather-page',
  templateUrl: './weather-page.component.html',
  styleUrls: ['./weather-page.component.css']
})
export class WeatherPageComponent implements OnInit {
	
	woeid: number = 0;

	constructor(
  		private route: ActivatedRoute
  	) { }

	ngOnInit() {
		this.woeid = this.route.snapshot.params['id'];
		// console.log("init",this.woeid);
	}

}
