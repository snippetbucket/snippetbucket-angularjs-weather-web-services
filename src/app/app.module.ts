import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { WeatherPageComponent } from './weather-page/weather-page.component';
import { WeatherComponent } from './common/weather/weather.component';

import { RoutesModule } from './routes/routes.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    WeatherPageComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    RoutesModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
