import { Component, OnInit, Input } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from "rxjs/operators";
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

	constructor(private http: Http,private route: ActivatedRoute) { }

	@Input() city : any[] = [];
	@Input() type : string = "";
	isList : boolean;
	@Input() city_id : number = null;

	private pri_data : Array<any>;
	data : Array<any> = [];

	ngOnInit() {

		// ADDED SUBSCRIBE FOR WNEN CHANGE THE PARAM OF SEARCH ROUTE TO AUTOMATIC DETACT THE CHANGE OF CITY NAME IN SEARCH
		this.city_id ? this.weatherById(this.city_id) : this.route.snapshot.params['key'] ? this.route.params.subscribe(params => {this.data = []; this.getData([[params.key]]);}) : this.getData(this.city);

		this.type == "list" ? this.isList =  true : this.isList =  false;

	}


	getData(cities){
		console.log("comp-getData",cities);
		for (let city of cities) {
			// console.log("for.........city",city)
			this.http.get('http://localhost/snippetbucket-angularjs-weather-web-services/api/weather.php?command=search&keyword='+city)
				.pipe(map(res=>{
							this.pri_data = this.extractData(res);
							this.getDetails(this.pri_data);
						})
				).subscribe();
		}
	}


	getDetails(data){
		// console.log("data>>>>>>>>",data);
		for (let entry of data) {
		    // console.log("entry>>>>>>>>",entry); // 1, "string", false
			this.http.get('http://localhost/snippetbucket-angularjs-weather-web-services/api/weather.php?command=location&woeid='+entry['woeid'])
			.pipe(map(res=>{
			 	this.data.push(this.extractData(res));
			  })
			).subscribe();
		}
	}

    private extractData(res: Response) {
    	// console.log("response",res);
    	let body;
    	if(res['_body'] !== "0"){	
			return body = res.json();
    	}
        return body || [];
    }

	weatherById(data){
		console.log("comp-weatherById",data);
		let obj: any[] = [[]];
		obj[0]['woeid'] = data;
		this.getDetails(obj);
	}

}
