import { Component } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	key : any[];
	constructor(private router: Router){}

	redirectSearch(key){
		console.log("call to redirectSearch funtion",key);
		this.key = key;
		this.router.navigate(['/search/'+key]);
	}
}
